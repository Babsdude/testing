<?php
namespace Babs\ShippingEvent\Controller\Adminhtml\Content;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Babs\ShippingEvent\Model\ShippingEventFactory;

class Save extends Action
{
    /**
     * Constants.
     */
    const ADMIN_RESOURCE = 'Babs_ShippingEvent::shipping_event_items';

    /**
     * ShippingEvent factory.
     *
     * @var \Babs\ShippingEvent\Model\ShippingEventFactory
     */
    protected $shippingEventModelFactory;

    /**
     * Save constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Babs\ShippingEvent\Model\ShippingEventFactory $shippingEventModelFactory
     */
    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        ShippingEventFactory $shippingEventModelFactory
    ) {
        $this->shippingEventModelFactory = $shippingEventModelFactory;
        parent::__construct($context);//, $coreRegistry);
    }

    /**
     * Save action.
     *
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $eventId = $this->getRequest()->getParam('event_id');

            if (empty($data['event_id'])) {
                $data['event_id'] = null;
            }

            $model = $this->shippingEventModelFactory->create()->load($eventId);
            if (!$model->getId() && $eventId) {
                $this->messageManager->addErrorMessage(__('This Shipping Event no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Shipping Event successfully.'));

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['event_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving Shipping Event.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['event_id' => $eventId]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
