<?php
namespace Babs\ShippingEvent\Controller\Adminhtml\Content;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;
use Babs\ShippingEvent\Model\ShippingEventFactory;

class Edit extends Action
{
    const ADMIN_RESOURCE = 'Babs_ShippingEvent::shipping_event_items';

    /**
     * Page factory.
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * ShippingEvent factory
     *
     * @var \Babs\ShippingEvent\Model\ShippingEventFactory
     */
    protected $shippingEventModelFactory;

    /**
     * Edit constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Babs\ShippingEvent\Model\ShippingEventFactory $shippingEventModelFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        ShippingEventFactory $shippingEventModelFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->shippingEventModelFactory = $shippingEventModelFactory;
        parent::__construct($context);
    }

    /**
     * Edit action.
     *
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('event_id');
        $model = $this->shippingEventModelFactory->create();

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Shipping Event no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Babs_ShippingEvent::shipping_event_items');

        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getName() : __('New Shipping Event'));

        return $resultPage;
    }
}
