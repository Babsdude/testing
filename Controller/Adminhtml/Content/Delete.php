<?php
namespace Babs\ShippingEvent\Controller\Adminhtml\Content;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;

class Delete extends Action
{
    const ADMIN_RESOURCE = 'Babs_ShippingEvent::shipping_event_items';

    /**
     * Page factory.
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Shipping event factory
     *
     * @var \Babs\ShippingEvent\Model\ShippingEventFactory
     */
    protected $shippingEventModelFactory;

    /**
     * Delete constructor.
     *
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Babs\ShippingEvent\Model\ShippingEventFactory $shippingEventModelFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Babs\ShippingEvent\Model\ShippingEventFactory $shippingEventModelFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->shippingEventModelFactory = $shippingEventModelFactory;
        parent::__construct($context);
    }

    /**
     * Delete action.
     *
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('event_id');

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->shippingEventModelFactory->create()->load($id);
                $model->delete();

                $this->messageManager->addSuccessMessage(__('The Shipping Event has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['event_id' => $id]);
            }
        }

        $this->messageManager->addErrorMessage(__('We can\'t find Shipping Event with such id.'));
        return $resultRedirect->setPath('*/*/');
    }
}
