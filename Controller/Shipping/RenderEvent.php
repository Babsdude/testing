<?php
namespace Babs\ShippingEvent\Controller\Shipping;

use Magento\Framework\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Babs\ShippingEvent\Model\ResourceModel\ShippingEventItem\CollectionFactory;

class RenderEvent extends Action\Action
{
    protected $collectionFactory;
    protected $resultJsonFactory;

    public function __construct(
        Action\Context $context,
        CollectionFactory $collectionFactory,
        JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Review point:: *DONE?* потрібно використовувати той же ui component і knockout для виведення цих даних
     * тут я невпевнений наскільки добре буде працювати це з ajax запитами, бо в попередніх версіях з цим була проблема
     */

    public function execute()
    {
        if ($this->getRequest()->isAjax())
        {
            $result = $this->resultJsonFactory->create();
            $collection = $this->collectionFactory->create();
            $collection->doJoin($this->getRequest()->getParam('quote_id'));
            return $result->setData($collection);
        }
        $this->_redirect('');
    }
}

