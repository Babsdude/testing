define([
    'jquery',
    'uiRegistry',
    'ko'
],function($, registry, ko) {
    'use strict';

    return function(actions) {
        return actions.extend({
            quoteId: ko.observable(),
            applyAction: function (actionIndex, rowIndex) {
                var quoteId = this.getQuoteId(rowIndex);
                this.setQuote(quoteId);
            },

            /**
             * Initialize observable
             * @returns {initObservable}
             */
            initObservable: function () {
                this._super()
                    .track([
                        'quoteId'
                    ]);

                return this;
            },

            /**
             * Sets quoteId.
             * If quoteId is set, display custom table
             * @param quoteId
             */
            setQuote: function(quoteId) {
                if(this.quoteId() === quoteId){
                    self.visibilityOn();
                }
                this.quoteId(quoteId);
            },

            /**
             * Get quoteId
             * @param rowIndex
             */
            getQuoteId: function(rowIndex) {
                var viewModel = registry.get('freeshipping_listing.freeshipping_listing_data_source');
                var itemQuoteId = viewModel.data.items[rowIndex].quote_id;

                return itemQuoteId;
            }
        });
    }
});