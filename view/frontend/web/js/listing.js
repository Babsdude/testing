define([
    'ko',
    'underscore',
    'Magento_Ui/js/grid/listing',
    'jquery'
], function (ko, _, Listing, $) {
    'use strict';

    return Listing.extend({
        quoteId: ko.observable(),
        visibilityFlag: ko.observable(false),
        defaults: {
            links: {
                quoteId: '${ $.name }.actions:quoteId'
            }
        },

        /**
         * Initialize
         */
        initialize: function () {
            self = this;
            this._super();
            this.listenForQuoteChange();
        },

        /**
         * Initialize observables
         * @returns {exports}
         */
        initObservable: function () {
            this._super()
                .track([
                    'quoteId'
                ]);

            return this;
        },

        /**
         * Visibility of custom table Off
         */
        visibilityOff: function() {
            this.visibilityFlag(false);
        },

        /**
         * Visibility of custom table On
         */
        visibilityOn: function() {
            this.visibilityFlag(true);
        },

        /**
         * Set items to display in table
         * @param items
         */
        setItemsList: function(items) {
            this.items = items;
        },

        /**
         * Listens for quoteId change.
         * If changed, does join and sets items to be displayed in table
         */
        listenForQuoteChange: function() {
            this.quoteId.subscribe(function(quoteId) {
                var itemsHere = [];
                $.ajax({
                    url: 'renderevent',
                    //showLoader: true,
                    data: {quote_id: quoteId},
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        data.items.forEach(function (item) {
                            itemsHere.push(item);
                        });
                        self.setItemsList(itemsHere);
                        self.visibilityOn();
                    },
                    error: function (request, error) {
                        console.log(request,error);

                        return;
                    }
                });
            });
        }
    });
});
