<?php
namespace Babs\ShippingEvent\Model;

use Babs\ShippingEvent\Model\ResourceModel\ShippingEvent as resourceShippingEvent;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Babs\ShippingEvent\Api\Data\ShippingEventInterface;

class ShippingEvent extends AbstractModel
    implements IdentityInterface, ShippingEventInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'babs_shipping_event';

    /**
     * Status constants
     */
    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    /**
     * @var string
     */
    protected $_cacheTag = 'babs_shipping_event';

    /**
     * @var string
     */
    protected $_eventPrefix = 'babs_shipping_event';

    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        /**
         * Review point:: *DONE* в таких випадках потрібно використовувати параметр який відповідає за назву класу, як наступним чином:
         * "ShippingEvent::class", і при цьому підключати клас через "use"
         **/
        $this->_init(resourceShippingEvent::class);
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . "_" . $this->getId()];
    }

    /**
     * Get event_id
     *
     * @return int
     */
    public function getEventId()
    {
        return $this->getData(self::EVENT_ID);
    }

    /**
     * Set event_id
     *
     * @param int $id
     * @return $this
     */
    public function setEventId($id)
    {
        return $this->setData(self::EVENT_ID, $id);
    }

    /**
     * Set base_total_amount
     *
     * @param int $totalAmount
     * @return $this
     */
    public function setBaseTotalAmount($totalAmount)
    {
        return $this->setData(self::BASE_TOTAL_AMOUNT, $totalAmount);
    }

    /**
     * Get base_total_amount
     *
     * @return int
     */
    public function getBaseTotalAmount()
    {
        return $this->getData(self::BASE_TOTAL_AMOUNT);
    }

    /**
     * Set created_at
     *
     * @param string $time
     * @return $this
     */
    public function setCreatedAt($time)
    {
        return $this->setData(self::CREATED_AT, $time);
    }

    /**
     * Get created_at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set updated_at
     *
     * @param string $time
     * @return $this
     */
    public function setUpdatedAt($time)
    {
        return $this->setData(self::UPDATED_AT, $time);
    }

    /**
     * Get updated_at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set order_id
     *
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Get order_id
     *
     * @return int
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * Set visibility
     *
     * @param int|bool $visibility
     * @return $this
     */
    public function setVisibility($visibility)
    {
        return $this->setData(self::VISIBILITY, $visibility);
    }

    /**
     * Get visibility
     *
     * @return int|bool
     */
    public function getVisibility()
    {
        return $this->getData(self::VISIBILITY);
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        return $this->setData(self::COMMENT, $comment);
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->getData(self::COMMENT);
    }
}