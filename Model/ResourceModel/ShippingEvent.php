<?php
namespace Babs\ShippingEvent\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class ShippingEvent extends AbstractDb
{
    /**
     * Initialize Model
     */
    protected function _construct()
    {
        $this->_init('shipping_event', 'event_id');
    }

}