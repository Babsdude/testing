<?php
namespace Babs\ShippingEvent\Model\ResourceModel\ShippingEvent;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Babs\ShippingEvent\Model\ShippingEvent as ModelShippingEvent;
use Babs\ShippingEvent\Model\ResourceModel\ShippingEvent as ResourceShippingEvent;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'event_id';

    /**
     * @var string
     */
    protected $_eventPrefix = 'babs_shipping_event_collection';

    /**
     * @var string
     */
    protected $_eventObject = 'event_collection';

    /**
     * Initialize Resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ModelShippingEvent::class, ResourceShippingEvent::class);
    }

}