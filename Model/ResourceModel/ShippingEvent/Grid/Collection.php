<?php
namespace Babs\ShippingEvent\Model\ResourceModel\ShippingEvent\Grid;

use Babs\ShippingEvent\Model\ResourceModel\ShippingEvent\Collection as EventCollection;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Babs\ShippingEvent\Model\ResourceModel\ShippingEvent;

class Collection extends EventCollection implements SearchResultInterface
{
    protected $_searchCriteria;

    protected $_aggregations;


    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $connection,
            $resource
        );
    }

    /**
     * Model and ResourceModel init.
     */
    public function _construct()
    {
        $this->_init(
            Document::class,
            ShippingEvent::class);
    }

    public function getAggregations()
    {
        return $this->_aggregations;
    }
    
    public function setAggregations($aggregations)
    {
        $this->_aggregations = $aggregations;
    }

    public function getSearchCriteria()
    {
        return null;
    }

    public function setSearchCriteria(
        SearchCriteriaInterface $searchCriteria)
    {
        $this->_searchCriteria = $searchCriteria;
        return $this;
    }

    public function getTotalCount()
    {
        return $this->getSize();
    }

    public function setTotalCount($totalCount)
    {
        return $this;
    }

    public function setItems(array $items = null)
    {
        return $this;
    }
}