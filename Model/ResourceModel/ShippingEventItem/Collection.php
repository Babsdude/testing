<?php
namespace Babs\ShippingEvent\Model\ResourceModel\ShippingEventItem;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Babs\ShippingEvent\Model\ShippingEventItem as ModelShippingEventItem;
use Babs\ShippingEvent\Model\ResourceModel\ShippingEventItem as ResourceShippingEventItem;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'item_id';

    /**
     * @var string
     */
    protected $_eventPrefix = 'babs_shipping_event_item_collection';

    /**
     * @var string
     */
    protected $_eventObject = 'item_collection';

    /**
     * Initialize Resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ModelShippingEventItem::class, ResourceShippingEventItem::class);
    }

    public function doJoin($quoteId)
    {
        $this->getSelect()->join(
            ['second_table' => 'shipping_event'],
            'main_table.' . 'shipping_event_id' . ' = second_table.' . 'event_id',
            []
        )->where("second_table.quote_id = " . $quoteId);
    }
}