<?php
namespace Babs\ShippingEvent\Model\ResourceModel\ShippingEventItem\Grid;

use Babs\ShippingEvent\Model\ResourceModel\ShippingEventItem\Collection as ItemCollection;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;
use Babs\ShippingEvent\Model\ResourceModel\ShippingEventItem;

class Collection extends ItemCollection implements SearchResultInterface
{
    protected $_aggregations;

    protected $searchCriteria;

    /**
     * Model and ResourceModel init.
     */
    public function _construct()
    {
        $this->_init(
            Document::class,
            ShippingEventItem::class);
    }
    public function getAggregations()
    {
        return $this->_aggregations;
    }

    public function setAggregations($aggregations)
    {
        $this->_aggregations = $aggregations;
    }

    public function getSearchCriteria()
    {
        return null;
    }

    public function setSearchCriteria(
        SearchCriteriaInterface $searchCriteria)
    {
        $this->searchCriteria = $searchCriteria;
        return $this;
    }

    public function getTotalCount()
    {
        return $this->getSize();
    }

    public function setTotalCount($totalCount)
    {
        return $this;
    }

    public function setItems(array $items = null)
    {
        return $this;
    }

}