<?php
namespace Babs\ShippingEvent\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ShippingEventItem extends AbstractDb
{
        /**
         * Review point:: *DONE* непотрібне підключення класу \Magento\Framework\Stdlib\DateTime
         * DONE = removed constructor altogether.
         */

    /**
     * Initialize Model
     */
    protected function _construct()
    {
        $this->_init('shipping_event_item', 'item_id');
    }

}