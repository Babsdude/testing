<?php
namespace Babs\ShippingEvent\Model;

use Babs\ShippingEvent\Api\Data\ShippingEventInterfaceFactory;
use Babs\ShippingEvent\Api\Data\ShippingEventSearchResultsInterfaceFactory;
use Babs\ShippingEvent\Api\Data\ShippingEventRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Babs\ShippingEvent\Model\ResourceModel\ShippingEvent\CollectionFactory;

class ShippingEventRepository implements ShippingEventRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var ShippingEventSearchResultsInterfaceFactory
     */
    protected $searchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * ShippingEventRepository constructor.
     * @param CollectionFactory $factory
     * @param CollectionProcessorInterface|null $collectionProcessor
     * @param ShippingEventSearchResultsInterfaceFactory $searchResultsInterfaceFactory
     */
    public function __construct(
        CollectionFactory $factory,
        CollectionProcessorInterface $collectionProcessor = null,
        ShippingEventSearchResultsInterfaceFactory $searchResultsInterfaceFactory
    ) {
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
        $this->searchResultFactory = $searchResultsInterfaceFactory;
        $this->collectionFactory = $factory;
    }

    /**
     * Get list.
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Babs\ShippingEvent\Api\Data\ShippingEventSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria)
            ->setItems($collection->getItems())
            ->setTotalCount($collection->getSize());

        return $searchResult;
    }
}