<?php
namespace Babs\ShippingEvent\Model;

use Babs\ShippingEvent\Model\ResourceModel\ShippingEventItem as resourceShippingEventItem;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Babs\ShippingEvent\Api\Data\ShippingEventItemInterface;

class ShippingEventItem extends AbstractModel
    implements  IdentityInterface, ShippingEventItemInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'babs_shipping_event_item';

    /**
     * @var string
     */
    protected $_cacheTag = 'babs_shipping_event_item';

    /**
     * @var string
     */
    protected $_eventPrefix = 'babs_shipping_event_item';

    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        /**
         * Review point:: *DONE* в таких випадках потрібно використовувати параметр який відповідає за назву класу, як наступним чином:
         * "ShippingEventItem::class", і при цьому підключати клас через "use"
         **/
        $this->_init(resourceShippingEventItem::class);
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . "_" . $this->getId()];
    }

    /**
     * Get item_id
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->getData(self::ITEM_ID);
    }

    /**
     * Set item_id
     *
     * @param int $id
     * @return $this
     */
    public function setItemId($id)
    {
        return $this->setData(self::ITEM_ID, $id);
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * Set product_id
     *
     * @param int $id
     * @return $this
     */
    public function setProductId($id)
    {
        return $this->setData(self::PRODUCT_ID, $id);
    }

    /**
     * Get shipping_event_id
     *
     * @return int
     */
    public function getShippingEventId()
    {
        return $this->getData(self::SHIPPING_EVENT_ID);
    }

    /**
     * Set shipping_event_id
     *
     * @param int $id
     * @return $this
     */
    public function setShippingEventId($id)
    {
        return $this->setData(self::SHIPPING_EVENT_ID, $id);
    }

}