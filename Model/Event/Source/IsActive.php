<?php
namespace Babs\ShippingEvent\Model\Event\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Babs\ShippingEvent\Model\ShippingEvent;

class IsActive implements OptionSourceInterface
{
    /**
     * Prepare options.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = [
            ShippingEvent::STATUS_DISABLED => __('Hidden'),
            ShippingEvent::STATUS_ENABLED => __('Revealed')
        ];

        $options = [];
        foreach($availableOptions as $key => $value)
            $options[] = ['label' => $value, 'value' => $key];

        return $options;
    }
}