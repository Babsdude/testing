<?php
namespace Babs\ShippingEvent\Model;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Babs\ShippingEvent\Model\ResourceModel\ShippingEvent\CollectionFactory;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider as uiDataProvider;

class DataProvider extends uiDataProvider
{
    protected $_collection;

    protected $_loadedData;

    /**
     * DataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param ReportingInterface $reporting
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ResourceModel\ShippingEvent\CollectionFactory $factory
     * @param RequestInterface $request
     * @param FilterBuilder $filterBuilder
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionFactory $factory,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        array $meta = [],
        array $data = []
    ) {
    $this->_collection = $factory->create();
    parent::__construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        $reporting,
        $searchCriteriaBuilder,
        $request,
        $filterBuilder,
        $meta,
        $data);
    }

    /**
     * Get data.
     *
     * @return array
     */
    public function getData()
    {
        if(isset($this->_loadedData))
            return $this->_loadedData;

        $items = $this->_collection->getItems();

        foreach($items as $item)
            $this->_loadedData[$item->getId()] = $item->getData();

        return $this->_loadedData;
    }
}