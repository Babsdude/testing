<?php
namespace Babs\ShippingEvent\Plugin;

use Babs\ShippingEvent\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;

class Description
{
    /**
     * Custom Helper
     *
     * @var \Babs\ShippingEvent\Helper\Data
     */
    protected $_helper;

    /**
     * Store manager. Used to get store currency.
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Description constructor.
     * @param Data $helper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Data $helper,
        StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Catalog\Block\Product\View\Description $subject
     * @param $result
     * @return mixed
     */
    public function afterGetProduct(\Magento\Catalog\Block\Product\View\Description $subject, $result)
    {
        if($this->_helper->getIsFreeShippingEnabled()){
            $price = $result->getFinalPrice();
            $limit = $this->_helper->getLimit();
            $status = $this->checkStatus($limit, $price);
            $text = $this->makeDescription($status);

            /**
             * Review point:: *DONE* непотрібно повертати стільки разів $result, так як він всеодно буде повернений в результаті.
             * Достатньо просто змінити тут значення description
             */
            return $result->setDescription($text);
        }
    }

    /**
     * Checks if freeshipping limit was passed.
     *
     * @param int|float $limit
     * @param int|float $price
     * @return bool|float
     */
    public function checkStatus($limit, $price)
    {
        $result = (float)$limit - (float)$price;
        if ($result >= 0) {
            /** Rounds and Displays float with 2 decimals after dot */
            $preparedResult = number_format((float)round($result,2), 2, '.', '');
            return $preparedResult;
        }
        /**
         * Review point:: *DONE* непотрібна умова else, достатньо просто повернути false
         */
        return false;
    }

    /**
     * Makes new product description.
     *
     * @param bool|float $status
     * @return string
     */
    public function makeDescription($status)
    {
        if($status) {
            $status = $status . ' ' . $this->_storeManager->getStore()
                ->getCurrentCurrency()->getCode() . ' ' .__('left to receive free shipping.');
        } else {
            $status = __('Free shipping available');
        }
        return $result = '<p>' . $status . '</p>';
    }
}
