<?php
namespace Babs\ShippingEvent\Observer;

use Babs\ShippingEvent\Model;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\Message\ManagerInterface;

class ShippingEventSave implements ObserverInterface
{
    /**
     * @var \Babs\ShippingEvent\Model\ShippingEventFactory
     */
    protected $_shippingEventFactory;

    /**
     * @var \Babs\ShippingEvent\Model\ShippingEventItemFactory
     */
    protected $_shippingEventItemFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_session;

    /**
     * @var Model\ResourceModel\ShippingEventFactory
     */
    protected $_resourceEvent;

    /**
     * @var Model\ResourceModel\ShippingEventItemFactory
     */
    protected $_resourceEventItem;

    protected $messageManager;

    /**
     * ShippingEventSave constructor.
     * @param Model\ShippingEventFactory $shippingEventFactory
     * @param Model\ShippingEventItemFactory $shippingEventItemFactory
     * @param Model\ResourceModel\ShippingEvent $resourceEvent
     * @param Model\ResourceModel\ShippingEventItem $resourceEventItem
     * @param ManagerInterface $messageManager
     * @param Session $session
     */
    public function __construct(
        Model\ShippingEventFactory $shippingEventFactory,
        Model\ShippingEventItemFactory $shippingEventItemFactory,
        Model\ResourceModel\ShippingEvent $resourceEvent,
        Model\ResourceModel\ShippingEventItem $resourceEventItem,
        ManagerInterface $messageManager,
        Session $session
    ) {
        $this->_session = $session;
        $this->_shippingEventItemFactory = $shippingEventItemFactory;
        $this->_shippingEventFactory = $shippingEventFactory;
        $this->_resourceEvent = $resourceEvent;
        $this->_resourceEventItem = $resourceEventItem;
        $this->messageManager = $messageManager;
    }


    public function execute(Observer $observer)
    {
        /** * Prepare variables */
        $product = $observer->getProduct();
        $total = $product->getQty() * $product->getFinalPrice();
        $modelEvent = $this->_shippingEventFactory->create();

        /** Save variables to shipping_event */

        $modelEvent->setBaseTotalAmount($total)
            ->setCreatedAt(date('Y-m-d H:i:s'));

        if($this->_session->getQuoteId()){
            $modelEvent->setQuoteId($this->_session->getQuoteId());
        } else {
            return;
        }

        /**
         * Review point:: *DONE* для збереження потрібно використовувати resource model
         **/

        try {
            $this->_resourceEvent->save($modelEvent);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }


        /** @var $modelEventId
         * Get id of inserted record in shipping_event table */
        $modelEventId = $modelEvent->getId();

        $modelEventItem = $this->_shippingEventItemFactory->create();

        /** Save variables to shipping_event_item */
        $modelEventItem->setShippingEventId($modelEventId)
            ->setProductId($product->getId());

        try {
            $this->_resourceEventItem->save($modelEventItem);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }
}