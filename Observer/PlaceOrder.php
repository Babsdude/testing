<?php
namespace Babs\ShippingEvent\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\Event\Observer;
use Babs\ShippingEvent\Api\Data\ShippingEventRepositoryInterface as Repository;
use Magento\Framework\Api\SearchCriteriaBuilder;

class PlaceOrder implements ObserverInterface
{
    /**
     * @var OrderInterface
     */
    protected $_order;

    /**
     * @var Repository
     */
    protected $repository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * PlaceOrder constructor.
     * @param OrderInterface $order
     * @param Repository $repository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        OrderInterface $order,
        Repository $repository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->repository = $repository;
        $this->_order = $order;
    }

    /**
     * Review point:: *DONE* потрібно замінити використання колекцій напряму на використання репозиторіїв (зараз репозиторії взагалі не реалізовані)
     * Також для фільтрації колекції використовувати SearchCriteria
     **/
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $orderId = $observer->getEvent()->getOrderIds();
        $order = $this->_order->load($orderId);
        $quoteId = $order->getQuoteId();

        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            'quote_id', $quoteId, 'eq'
        )->create();

        $list = $this->repository->getList($searchCriteria);
        $items = $list->getItems();

        foreach($items as $item)
        {
            $item->setUpdatedAt(date('Y-m-d H:i:s'))
                ->setOrderId($orderId[0]);
            $item->getResource()->save($item);
        }
    }
}