<?php
namespace Babs\ShippingEvent\Api\Data;

interface ShippingEventItemInterface
{
    /**
     * Constant variables for set/get data
     */
    const ITEM_ID      = 'item_id';
    const SHIPPING_EVENT_ID  = 'shipping_event_id';
    const PRODUCT_ID = 'product_id';

    /**
     * Set item_id
     *
     * @param $id
     * @return mixed
     */
    public function setItemId($id);

    /**
     * Get item_id
     *
     * @return mixed
     */
    public function getItemId();

    /**
     * Set shipping_event_id
     *
     * @param $id
     * @return mixed
     */
    public function setShippingEventId($id);

    /**
     * Get shipping_event_id
     *
     * @return mixed
     */
    public function getShippingEventId();

    /**
     * Set product_id
     *
     * @param $id
     * @return mixed
     */
    public function setProductId($id);

    /**
     * Get product_id
     *
     * @return mixed
     */
    public function getProductId();
}