<?php
namespace Babs\ShippingEvent\Api\Data;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ShippingEventRepositoryInterface
{
    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}