<?php
namespace Babs\ShippingEvent\Api\Data;

interface ShippingEventInterface
{
    /**
     * Constant variables for set/get data
     */
    const EVENT_ID      = 'event_id';
    const BASE_TOTAL_AMOUNT  = 'base_total_amount';
    const CREATED_AT = 'created_at';
    const UPDATED_AT   = 'updated_at';
    const ORDER_ID      = 'order_id';
    const VISIBILITY    = 'visibility';
    const COMMENT       = 'comment';

    /**
     * Set event_id
     *
     * @param $id
     * @return mixed
     */
    public function setEventId($id);

    /**
     * Get event_id
     *
     * @return mixed
     */
    public function getEventId();

    /**
     * Set base_total_amount
     *
     * @param $totalAmount
     * @return mixed
     */
    public function setBaseTotalAmount($totalAmount);

    /**
     * Get base_total_amount
     *
     * @return mixed
     */
    public function getBaseTotalAmount();

    /**
     * Set created_at
     *
     * @param $time
     * @return mixed
     */
    public function setCreatedAt($time);

    /**
     * Get created_at
     *
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * Set updated_at
     *
     * @param $time
     * @return mixed
     */
    public function setUpdatedAt($time);

    /**
     * Get updated_at
     *
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * Set order_id
     *
     * @param $orderId
     * @return mixed
     */
    public function setOrderId($orderId);

    /**
     * Get order_id
     *
     * @return mixed
     */
    public function getOrderId();

    /**
     * Set visibility
     *
     * @param $visibility
     * @return mixed
     */
    public function setVisibility($visibility);

    /**
     * Get visibility
     *
     * @return mixed
     */
    public function getVisibility();

    /**
     * Set comment
     *
     * @param $comment
     * @return mixed
     */
    public function setComment($comment);

    /**
     * Get comment
     *
     * @return mixed
     */
    public function getComment();
}