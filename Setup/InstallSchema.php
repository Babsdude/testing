<?php/** Review point:: *DONE* не повинно бути пустих ліній перед 'namespace' **/
namespace Babs\ShippingEvent\Setup;

use Magento\Framework\Setup;

class InstallSchema implements Setup\InstallSchemaInterface
{
    public function install(
        Setup\SchemaSetupInterface $setup,
        Setup\ModuleContextInterface $context
    ) { /** Review point:: *DONE* у випадку якщо кілька параметрів на різних рядках, то закриваюча дужка повинна бути на новій ліній (причому на одній з фігурною дужкою) **/
        $installer = $setup;
        $installer->startSetup();
        /** Review point:: *DONE*
         * Якось дивно тут використовуються умови "if($installer->getTable('shipping_event'))" і "if (!$installer->tableExists('shipping_event'))"
         * Якщо робити перевірку і видаляти таблицю, то тоді не викликати наступну дубльовану перевірку перед створенням таблиці,
         * а використовувати при видаленні таблиці "try {} catch"
         **/
        try {
            if($installer->getTable('shipping_event'))
            {
                $installer->getConnection()
                    ->dropTable($installer->getTable('shipping_event'));
            }
        } catch (\Exception $e) {

        }
        /**
         * Create table 'shipping_event'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('shipping_event')
        )
        ->addColumn(
            'event_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            NULL,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true
            ]
        )
        ->addColumn(
            'base_total_amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            NULL,
            ['nullable' => true]
        )
        ->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            NULL,
            []
        )
        ->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            NULL,
            []
        )
        ->addColumn(
            'quote_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            NULL,
            ['nullable' => true]
        )
        ->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            NULL,
            ['nullable' => true]
        )
        ->addColumn(
            'visibility',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            NULL,
            ['nullable' => true]
        )
        ->addColumn(
            'comment',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            NULL,
            ['nullable' => true]
        );
        $installer->getConnection()->createTable($table);
        $installer->getConnection()->addIndex(
            $installer->getTable('shipping_event'),
            $setup->getIdxName(
                $installer->getTable('shipping_event'),
                ['comment'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['comment'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );
        try {
            if($installer->getTable('shipping_event_item'))
            {
                $installer->getConnection()
                    ->dropTable($installer->getTable('shipping_event_item'));
            }
        } catch (\Exception $e) {

        }
        /**
         * Create table 'shipping_event_item'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('shipping_event_item')
        )
            ->addColumn(
                'item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                NULL,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true
                ]
            )
            ->addColumn(
                'shipping_event_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                NULL,
                ['nullable' => false]
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                NULL,
                ['nullable' => false]
            );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}