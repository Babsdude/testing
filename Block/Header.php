<?php
namespace Babs\ShippingEvent\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Pricing\Helper\Data;
use Babs\ShippingEvent\Helper\Data as babsData;

class Header extends Template
{
    /**
     * Custom helper.
     *
     * @var \Babs\ShippingEvent\Helper\Data
     */
    protected $_helper;

    /**
     * Pricing helper.
     *
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_pricingHelper;
    /**
     * Header constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Data $pricingHelper,
        babsData $helper,
        array $data = [])
    {
        $this->_pricingHelper = $pricingHelper;
        $this->_helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Returns limit with currency sign.
     *
     * @return string
     */
    public function getFreeshippingLimit()
    {
        $result = $this->_pricingHelper->currency($this->_helper->getLimit(), true, false);
        return $result;
    }
    public function canShow()
    {
        return $this->_helper->getIsFreeShippingEnabled();
    }
}