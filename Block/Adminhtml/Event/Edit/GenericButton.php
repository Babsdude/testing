<?php
namespace Babs\ShippingEvent\Block\Adminhtml\Event\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Magento\Backend\Block\Widget\Context;
use Babs\ShippingEvent\Model\ShippingEventFactory;

class GenericButton implements ButtonProviderInterface
{
    protected $context;

    protected $shippingEventFactory;

    public function __construct(
        Context $context,
        ShippingEventFactory $shippingEventFactory
    ) {
        $this->context = $context;
        $this->shippingEventFactory = $shippingEventFactory;
    }

    public function getEventId()
    {
        try {
            $shippingEventId = $this->context->getRequest()->getParam('event_id');
            return $this->shippingEventFactory->create()->load($shippingEventId)->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
        }
        return null;
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }

    public function getButtonData()
    {
        return [];
    }
}