<?php
namespace Babs\ShippingEvent\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * Constants, same name as backend fields
     */
    const XML_PATH_ENABLED = 'minimum_shipping_amount/general/enabled';
    const XML_PATH_LIMIT = 'minimum_shipping_amount/general/limit';

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scope
     */
    public function __construct(Context $context)
    {
        /** Review point:: *DONE* неможна переініціалізовувати класи які вже бути використані в батьківських класах (це також ламає setup:di:compile)
         * У даному випадку \Magento\Framework\App\Config\ScopeConfigInterface є в батькіських класах як $this->scopeConfig
         **/
        parent::__construct($context);
    }

    /**
     * Gets value from admin, enable freeshipping field
     *
     * @return mixed
     */
    public function getIsFreeShippingEnabled()
    {
        $data = $this->scopeConfig->getValue(self::XML_PATH_ENABLED, ScopeInterface::SCOPE_STORE);
        return $data;
    }

    /**
     * Gets value from admin, set freeshipping limit field
     *
     * @return mixed
     */
    public function getLimit()
    {
        $data = $this->scopeConfig->getValue(self::XML_PATH_LIMIT, ScopeInterface::SCOPE_STORE);
        return $data;
    }

}
